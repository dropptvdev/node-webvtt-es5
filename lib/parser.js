'use strict';
/**
 * See spec: https://www.w3.org/TR/webvtt1/#file-structure
 */

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ParserError(message, error) {
  this.message = message;
  this.error = error;
}

ParserError.prototype = Object.create(Error.prototype);
var TIMESTAMP_REGEXP = /([0-9]{1,2})?:?([0-9]{2}):([0-9]{2}\.[0-9]{2,3})/;

function parse(input, options) {
  if (!options) {
    options = {};
  }

  var _options = options,
      _options$meta = _options.meta,
      meta = _options$meta === void 0 ? false : _options$meta,
      _options$strict = _options.strict,
      strict = _options$strict === void 0 ? true : _options$strict;

  if (typeof input !== 'string') {
    throw new ParserError('Input must be a string');
  }

  input = input.trim();
  input = input.replace(/\r\n/g, '\n');
  input = input.replace(/\r/g, '\n');
  var parts = input.split('\n\n');
  var header = parts.shift();

  if (!header.startsWith('WEBVTT')) {
    throw new ParserError('Must start with "WEBVTT"');
  }

  var headerParts = header.split('\n');
  var headerComments = headerParts[0].replace('WEBVTT', '');

  if (headerComments.length > 0 && headerComments[0] !== ' ' && headerComments[0] !== '\t') {
    throw new ParserError('Header comment must start with space or tab');
  } // nothing of interests, return early


  if (parts.length === 0 && headerParts.length === 1) {
    return {
      valid: true,
      strict: strict,
      cues: [],
      errors: []
    };
  }

  if (!meta && headerParts.length > 1 && headerParts[1] !== '') {
    throw new ParserError('Missing blank line after signature');
  }

  var _parseCues = parseCues(parts, strict),
      cues = _parseCues.cues,
      errors = _parseCues.errors;

  if (strict && errors.length > 0) {
    throw errors[0];
  }

  var headerMeta = meta ? parseMeta(headerParts) : null;
  var result = {
    valid: errors.length === 0,
    strict: strict,
    cues: cues,
    errors: errors
  };

  if (meta) {
    result.meta = headerMeta;
  }

  return result;
}

function parseMeta(headerParts) {
  var meta = {};
  headerParts.slice(1).forEach(function (header) {
    var _header$split$map = header.split(':').map(function (t) {
      return t.trim();
    }),
        _header$split$map2 = _slicedToArray(_header$split$map, 2),
        key = _header$split$map2[0],
        value = _header$split$map2[1];

    meta[key] = value;
  });
  return Object.keys(meta).length > 0 ? meta : null;
}

function parseCues(cues, strict) {
  var errors = [];
  var parsedCues = cues.map(function (cue, i) {
    try {
      return parseCue(cue, i, strict);
    } catch (e) {
      errors.push(e);
      return null;
    }
  }).filter(Boolean);
  return {
    cues: parsedCues,
    errors: errors
  };
}
/**
 * Parse a single cue block.
 *
 * @param {array} cue Array of content for the cue
 * @param {number} i Index of cue in array
 *
 * @returns {object} cue Cue object with start, end, text and styles.
 *                       Null if it's a note
 */


function parseCue(cue, i, strict) {
  var identifier = '';
  var start = 0;
  var end = 0.01;
  var text = '';
  var styles = ''; // split and remove empty lines

  var lines = cue.split('\n').filter(Boolean);

  if (lines.length > 0 && lines[0].trim().startsWith('NOTE')) {
    return null;
  }

  if (lines.length === 1 && !lines[0].includes('-->')) {
    throw new ParserError("Cue identifier cannot be standalone (cue #".concat(i, ")"));
  }

  if (lines.length > 1 && !(lines[0].includes('-->') || lines[1].includes('-->'))) {
    var msg = "Cue identifier needs to be followed by timestamp (cue #".concat(i, ")");
    throw new ParserError(msg);
  }

  if (lines.length > 1 && lines[1].includes('-->')) {
    identifier = lines.shift();
  }

  var times = lines[0].split(' --> ');

  if (times.length !== 2 || !validTimestamp(times[0]) || !validTimestamp(times[1])) {
    throw new ParserError("Invalid cue timestamp (cue #".concat(i, ")"));
  }

  start = parseTimestamp(times[0]);
  end = parseTimestamp(times[1]);

  if (strict) {
    if (start > end) {
      throw new ParserError("Start timestamp greater than end (cue #".concat(i, ")"));
    }

    if (end <= start) {
      throw new ParserError("End must be greater than start (cue #".concat(i, ")"));
    }
  }

  if (!strict && end < start) {
    throw new ParserError("End must be greater or equal to start when not strict (cue #".concat(i, ")"));
  } // TODO better style validation


  styles = times[1].replace(TIMESTAMP_REGEXP, '').trim();
  lines.shift();
  text = lines.join('\n');

  if (!text) {
    return false;
  }

  return {
    identifier: identifier,
    start: start,
    end: end,
    text: text,
    styles: styles
  };
}

function validTimestamp(timestamp) {
  return TIMESTAMP_REGEXP.test(timestamp);
}

function parseTimestamp(timestamp) {
  var matches = timestamp.match(TIMESTAMP_REGEXP);
  var secs = parseFloat(matches[3]);
  secs += parseFloat(matches[2]) * 60; // mins

  secs += parseFloat(matches[1] || 0) * 60 * 60; // hours
  // secs += parseFloat(matches[4]);

  return secs;
}

module.exports = {
  ParserError: ParserError,
  parse: parse
};
