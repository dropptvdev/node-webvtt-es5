'use strict';

var segment = require('./segmenter').segment;

function hlsSegment(input, segmentLength, startOffset) {
  if (typeof startOffset === 'undefined') {
    startOffset = '900000';
  }

  var segments = segment(input, segmentLength);
  var result = [];
  segments.forEach(function (seg, i) {
    var content = "WEBVTT\nX-TIMESTAMP-MAP=MPEGTS:".concat(startOffset, ",LOCAL:00:00:00.000\n\n").concat(printableCues(seg.cues), "\n");
    var filename = generateSegmentFilename(i);
    result.push({
      filename: filename,
      content: content
    });
  });
  return result;
}

function hlsSegmentPlaylist(input, segmentLength) {
  var segmented = segment(input, segmentLength);
  var printable = printableSegments(segmented);
  var longestSegment = Math.round(findLongestSegment(segmented));
  var template = "#EXTM3U\n#EXT-X-TARGETDURATION:".concat(longestSegment, "\n#EXT-X-VERSION:3\n#EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-PLAYLIST-TYPE:VOD\n").concat(printable, "\n#EXT-X-ENDLIST\n");
  return template;
}

function pad(num, n) {
  var padding = '0'.repeat(Math.max(0, n - num.toString().length));
  return "".concat(padding).concat(num);
}

function generateSegmentFilename(index) {
  return "".concat(index, ".vtt");
}

function printableSegments(segments) {
  var result = [];
  segments.forEach(function (seg, i) {
    result.push("#EXTINF:".concat(seg.duration.toFixed(5), ",\n").concat(generateSegmentFilename(i)));
  });
  return result.join('\n');
}

function findLongestSegment(segments) {
  var max = 0;
  segments.forEach(function (seg) {
    if (seg.duration > max) {
      max = seg.duration;
    }
  });
  return max;
}

function printableCues(cues) {
  var result = [];
  cues.forEach(function (cue) {
    result.push(printableCue(cue));
  });
  return result.join('\n\n');
}

function printableCue(cue) {
  var printable = [];

  if (cue.identifier) {
    printable.push(cue.identifier);
  }

  var start = printableTimestamp(cue.start);
  var end = printableTimestamp(cue.end);
  var styles = cue.styles ? "".concat(cue.styles) : ''; // always add a space after end timestamp, otherwise JWPlayer will not
  // handle cues correctly

  printable.push("".concat(start, " --> ").concat(end, " ").concat(styles));
  printable.push(cue.text);
  return printable.join('\n');
}

function printableTimestamp(timestamp) {
  var ms = (timestamp % 1).toFixed(3);
  timestamp = Math.round(timestamp - ms);
  var hours = Math.floor(timestamp / 3600);
  var mins = Math.floor((timestamp - hours * 3600) / 60);
  var secs = timestamp - hours * 3600 - mins * 60; // TODO hours aren't required by spec, but we include them, should be config

  var hourString = "".concat(pad(hours, 2), ":");
  return "".concat(hourString).concat(pad(mins, 2), ":").concat(pad(secs, 2), ".").concat(pad(ms * 1000, 3));
}

module.exports = {
  hlsSegment: hlsSegment,
  hlsSegmentPlaylist: hlsSegmentPlaylist
};
