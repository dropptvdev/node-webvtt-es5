'use strict';
/**
 * See spec: https://www.w3.org/TR/webvtt1/#file-structure
 */

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function CompilerError(message, error) {
  this.message = message;
  this.error = error;
}

CompilerError.prototype = Object.create(Error.prototype);

function compile(input) {
  if (!input) {
    throw new CompilerError('Input must be non-null');
  }

  if (_typeof(input) !== 'object') {
    throw new CompilerError('Input must be an object');
  }

  if (Array.isArray(input)) {
    throw new CompilerError('Input cannot be array');
  }

  if (!input.valid) {
    throw new CompilerError('Input must be valid');
  }

  var output = 'WEBVTT\n';
  input.cues.forEach(function (cue) {
    output += '\n';
    output += compileCue(cue);
    output += '\n';
  });
  return output;
}
/**
 * Compile a single cue block.
 *
 * @param {array} cue Array of content for the cue
 *
 * @returns {object} cue Cue object with start, end, text and styles.
 *                       Null if it's a note
 */


function compileCue(cue) {
  // TODO: check for malformed JSON
  if (_typeof(cue) !== 'object') {
    throw new CompilerError('Cue malformed: not of type object');
  }

  if (typeof cue.identifier !== 'string' && typeof cue.identifier !== 'number' && cue.identifier !== null) {
    throw new CompilerError("Cue malformed: identifier value is not a string.\n    ".concat(JSON.stringify(cue)));
  }

  if (isNaN(cue.start)) {
    throw new CompilerError("Cue malformed: null start value.\n    ".concat(JSON.stringify(cue)));
  }

  if (isNaN(cue.end)) {
    throw new CompilerError("Cue malformed: null end value.\n    ".concat(JSON.stringify(cue)));
  }

  if (cue.start >= cue.end) {
    throw new CompilerError("Cue malformed: start timestamp greater than end\n    ".concat(JSON.stringify(cue)));
  }

  if (typeof cue.text !== 'string') {
    throw new CompilerError("Cue malformed: null text value.\n    ".concat(JSON.stringify(cue)));
  }

  if (typeof cue.styles !== 'string') {
    throw new CompilerError("Cue malformed: null styles value.\n    ".concat(JSON.stringify(cue)));
  }

  var output = '';

  if (cue.identifier.length > 0) {
    output += "".concat(cue.identifier, "\n");
  }

  var startTimestamp = convertTimestamp(cue.start);
  var endTimestamp = convertTimestamp(cue.end);
  output += "".concat(startTimestamp, " --> ").concat(endTimestamp);
  output += cue.styles ? " ".concat(cue.styles) : '';
  output += "\n".concat(cue.text);
  return output;
}

function convertTimestamp(time) {
  var hours = pad(calculateHours(time), 2);
  var minutes = pad(calculateMinutes(time), 2);
  var seconds = pad(calculateSeconds(time), 2);
  var milliseconds = pad(calculateMs(time), 3);
  return "".concat(hours, ":").concat(minutes, ":").concat(seconds, ".").concat(milliseconds);
}

function pad(num, zeroes) {
  var output = "".concat(num);

  while (output.length < zeroes) {
    output = "0".concat(output);
  }

  return output;
}

function calculateHours(time) {
  return Math.floor(time / 60 / 60);
}

function calculateMinutes(time) {
  return Math.floor(time / 60) % 60;
}

function calculateSeconds(time) {
  return Math.floor(time % 60);
}

function calculateMs(time) {
  return Math.floor(time % 1 * 1000);
}

module.exports = {
  CompilerError: CompilerError,
  compile: compile
};
