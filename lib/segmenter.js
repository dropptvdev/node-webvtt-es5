'use strict';

var parse = require('./parser').parse;

function segment(input, segmentLength) {
  segmentLength = segmentLength || 10;
  var parsed = parse(input);
  var segments = [];
  var cues = [];
  var queuedCue = null;
  var currentSegmentDuration = 0;
  var totalSegmentsDuration = 0;
  /**
   * One pass segmenting of cues
   */

  parsed.cues.forEach(function (cue, i) {
    var firstCue = i === 0;
    var lastCue = i === parsed.cues.length - 1;
    var start = cue.start;
    var end = cue.end;
    var nextStart = lastCue ? Infinity : parsed.cues[i + 1].start;
    var cueLength = firstCue ? end : end - start;
    var silence = firstCue ? 0 : start - parsed.cues[i - 1].end;
    currentSegmentDuration = currentSegmentDuration + cueLength + silence;
    debug('------------');
    debug("Cue #".concat(i, ", segment #").concat(segments.length + 1));
    debug("Start ".concat(start));
    debug("End ".concat(end));
    debug("Length ".concat(cueLength));
    debug("Total segment duration = ".concat(totalSegmentsDuration));
    debug("Current segment duration = ".concat(currentSegmentDuration));
    debug("Start of next = ".concat(nextStart)); // if there's a boundary cue queued, push and clear queue

    if (queuedCue) {
      cues.push(queuedCue);
      currentSegmentDuration += queuedCue.end - totalSegmentsDuration;
      queuedCue = null;
    }

    cues.push(cue); // if a cue passes a segment boundary, it appears in both

    var shouldQueue = nextStart - end < segmentLength && silence < segmentLength && currentSegmentDuration > segmentLength;

    if (shouldSegment(totalSegmentsDuration, segmentLength, nextStart, silence)) {
      var duration = segmentDuration(lastCue, end, segmentLength, currentSegmentDuration, totalSegmentsDuration);
      segments.push({
        duration: duration,
        cues: cues
      });
      totalSegmentsDuration += duration;
      currentSegmentDuration = 0;
      cues = [];
    } else {
      shouldQueue = false;
    }

    if (shouldQueue) {
      queuedCue = cue;
    }
  });
  return segments;
}

function shouldSegment(total, length, nextStart, silence) {
  // this is stupid, but gets one case fixed...
  var x = alignToSegmentLength(silence, length);
  var nextCueIsInNextSegment = silence <= length || x + total < nextStart;
  return nextCueIsInNextSegment && nextStart - total >= length;
}

function segmentDuration(lastCue, end, length, currentSegment, totalSegments) {
  var duration = length;

  if (currentSegment > length) {
    duration = alignToSegmentLength(currentSegment - length, length);
  } // make sure the last cue covers the whole time of the cues


  if (lastCue) {
    duration = parseFloat((end - totalSegments).toFixed(2));
  } else {
    duration = Math.round(duration);
  }

  return duration;
}

function alignToSegmentLength(n, segmentLength) {
  n += segmentLength - n % segmentLength;
  return n;
}

var debugging = false;
/* istanbul ignore next */

function debug(m) {
  if (debugging) {
    console.log(m);
  }
}

module.exports = {
  segment: segment
};
