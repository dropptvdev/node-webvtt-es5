#!/usr/bin/env node
'use strict';

var fs = require('fs');

var path = require('path');

var hls = require('../lib/hls');

var program = require('commander');

program.version('0.0.1').usage('[options] <webvtt file>').option('-t, --target-duration [duration]', 'Target duration for each segment in secods, defaults to 10', parseInt, 10).option('-o, --output-directory [dir]', 'Output directory for segments and playlist').option('-v, --verbose', 'Chatty output').option('-s, --silent', 'No output').parse(process.argv);
var input = program.args;
var t = program.targetDuration || 10;
var outputDir = program.outputDirectory || './';
log('Hi there! Let’s try and parse and segment a webvtt file, shall we');
log("Output directory is: ".concat(outputDir));
log("Target duration is: ".concat(t));

if (input.length > 1) {
  fail('Too many inputs, only supports one file');
}

if (input.length === 0) {
  fail('Missing input file');
}

var file = input[0];
var content = '';

try {
  content = read(file);
  log("Read \"".concat(input, "\""));
} catch (e) {
  fail("Could not read \"".concat(input, "\""), e);
}

var playlist = '';

try {
  log("Creating playlist for \"".concat(input, "\" with ").concat(t, " sec segments"));
  playlist = hls.hlsSegmentPlaylist(content, t);
} catch (e) {
  fail("Unable to create playlist for \"".concat(input, "\""), e);
}

var playlistFilename = 'playlist.m3u8';
var target = path.join(outputDir, playlistFilename);

try {
  log("Writing ".concat(playlist.length, " bytes (utf-8) to \"").concat(playlistFilename, "\""));
  fs.writeFileSync(target, playlist, 'utf-8');
  log("Wrote \"".concat(playlistFilename, "\""));
} catch (e) {
  fail("Unable to write playlist \"".concat(target, "\""), e);
}

var segments = [];

try {
  log("Segmenting \"".concat(input, "\" (").concat(content.length, " bytes) with ").concat(t, " sec segments"));
  segments = hls.hlsSegment(content, t);
} catch (e) {
  fail("Unable to segment playlist for \"".concat(input, "\""), e);
}

var n = segments.length;
var dotdotdot = false;
log("Writing ".concat(n, " segments"));
segments.forEach(function (segment, i) {
  var filename = segment.filename;
  var targetFile = path.join(outputDir, filename);

  try {
    fs.writeFileSync(targetFile, segment.content, 'utf-8');
  } catch (e) {
    fail("Failed writing ".concat(filename, ", aborting. Wrote ").concat(i + 1, " segments."), e);
  }

  var range = 5;

  if (0 <= i && i < range || n - range <= i && i <= n) {
    log("Wrote segment ".concat(targetFile));
  } else {
    if (!dotdotdot) {
      log('...');
      dotdotdot = true;
    }
  }
});
log('Finished writing segments');
/** Helpers **/

function read(targetFile) {
  log("Trying to read \"".concat(targetFile, "\""));
  var stats = fs.statSync(targetFile);

  if (!stats.isFile()) {
    fail("\"".concat(targetFile, "\" is not a file"));
  }

  log("\"".concat(targetFile, "\" is a file, reading and assuming UTF-8"));
  var data = fs.readFileSync(targetFile, 'utf-8');
  data = data.replace(/^\uFEFF/, '');
  return data;
}

function log(m) {
  if (program.verbose && !program.silent) {
    console.log(m);
  }
}

function fail(m, e) {
  if (!program.silent) {
    console.log(m);

    if (e) {
      console.log("Exception: ".concat(e.stack));
    }
  }

  process.exit(1);
}
